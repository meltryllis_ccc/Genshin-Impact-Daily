# 原神动态抓取

#### 介绍
谷歌扩展：抓取原神官方在B站的动态<br>
自用 ~~翻译：不负责修Bug~~

#### 安装
在Chrome地址栏输入chrome://extensions/
选择"下载已解压的扩展程序"
![avatar](./img/readme/1.png)<br>
选中项目文件夹

#### 使用
点击扩展列表的图标即可
![avatar](./img/readme/2.png)


#### 参考
[鹰角蹲饼器](https://github.com/LiuZiYang1/Dun-Cookie)