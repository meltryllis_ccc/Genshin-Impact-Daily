let popup = {
    showList(cardList) {
        if (cardList != null && cardList.length > 0) {
            let html = "";
            cardList.map(x => {
                if (x.source == "bili") {
                    // console.log(x.content);
                    //2动态 4无图动态 8视频 64专栏
                    html += `<div class="card" data-type="0">
                    <div class="head">
                       <span class="left">
                        <img src="../img/ico/bili.ico">
                        <span class="title"></span>
                       </span>
                        <span class="time">${common.timeStampToTime(x.timeStamp)}</span>
                        <span class="btn" data-url="${x.url}">查看详情</span>
                    </div>
                    <div class="content">
                    <div>${x.content.replace(/\n/g, "<br/>")}</div><div class="imgarea ${x.image ? '' : 'hide'}"><img src="${x.image ? x.image : ''}"></div></div>
                        </div>`;
                }
            });
            $("#dynamicContent").html(html);
        }

        //查看详情按钮
        let card = document.querySelectorAll('.card .btn');
        card.forEach(item => {
            item.addEventListener('click', event => {
                chrome.tabs.create({ url: event.currentTarget.dataset.url });
            });
        });
    },

    getBackgroundData() {
        let info = chrome.extension.getBackgroundPage();
        let { biliCardList = [], weiboCardList = [] } = info.backgroundInfo.allCardList;
        let cardList = [...biliCardList, ...weiboCardList];
        cardList.sort(function(x, y) {
            return y.timeStamp - x.timtStamp; //降序
        });

        return cardList;
    }
}

//通用函数
let common = {
    //时间戳转时间
    timeStampToTime(timestamp) {
        var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return Y + M + D + h + m + s;

    }
}

window.onload = function() {
    let info = chrome.extension.getBackgroundPage();
    $("#title").html("原神日报&nbsp;&nbsp;" + "<span style='color: #1E9FFF'>Version&nbsp;" + info.backgroundInfo.version + "</span>");

    //跳转功能
    let sourcePage = document.querySelectorAll('.URL');
    sourcePage.forEach(item => {
        item.addEventListener('click', event => {
            chrome.tabs.create({ url: event.currentTarget.dataset.url });
        });
    });

    let cardList = popup.getBackgroundData();
    popup.showList(cardList);
    chrome.storage.local.get(['settings'], result => {
        let settings = result.settings;

        setInterval(() => {
            popup.getBackgroundData();
            popup.showList(cardList);
        }, settings.refreshFrequency);
    });
}